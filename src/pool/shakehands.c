/**
 * Azalea Chatting Server
 *
 * Copyright (C) 2013-2015 Rex Lee <duguying2008@gmail.com>
 *
 * This program is free and opensource software; 
 * you can redistribute it and/or modify
 * it under the terms of the GNU General Public License
 */

#include "ichat.h"

/**
 * send shakehands infomation
 * @param  user_id user id
 * @return         IOK/IERROR
 */
int shake_send(uid user_id){
	return IOK;
}

/**
 * parse shakehands information
 * @param  message the shakehands information received
 * @return         result of parse
 */
int shake_parse(const char* message){
	return IOK;
}
